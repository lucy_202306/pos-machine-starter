package pos.machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);

        Receipt receipt = calculateCost(receiptItems);
        String info = renderReceipt(receipt);
        return info;
    }

    public List<Item> loadAllItems() {
        return ItemsLoader.loadAllItems();
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {
        HashMap<String, Integer> map = new HashMap<>();
        for (String barcode : barcodes) {
            if (map.containsKey(barcode)) {
                map.put(barcode, map.get(barcode) + 1);
            } else {
                map.put(barcode, 1);
            }
        }

        List<Item> items = loadAllItems();
        List<ReceiptItem> ret = new ArrayList<>();
        ReceiptItem receiptItem = null;
        for (Item item : items) {
            receiptItem = new ReceiptItem(item.getName(), map.get(item.getBarcode()), item.getPrice());
            ret.add(receiptItem);
        }

        return ret;
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        receiptItems = calculateItemsCost(receiptItems);
        int totalPrice = calculateTotalPrice(receiptItems);
        Receipt receipt = new Receipt(receiptItems, totalPrice);
        return receipt;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubTotal(receiptItem.getQuantity() * receiptItem.getUnitPrice());
        }
        return receiptItems;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int totalPrice = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            totalPrice += receiptItem.getSubTotal();
        }
        return totalPrice;
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        String info = generateReceipt(itemsReceipt, receipt.getTotalPrice());
        String ret = "***<store earning no money>Receipt***\n" + info + "**********************";
        return ret;
    }

    public String generateItemsReceipt(Receipt receipt) {
        List<ReceiptItem> items = receipt.getReceiptItems();
        String ret = "";
        for (ReceiptItem item : items) {
            ret += ("Name: " + item.getName() + ", Quantity: " + item.getQuantity() + ", Unit price: " + item.getUnitPrice() + " (yuan), Subtotal: " + item.getSubTotal() + " (yuan)\n");
        }
        return ret;
    }

    public String generateReceipt(String itemsReceipt, int totalPrice) {
        itemsReceipt += "---------------------\n";
        itemsReceipt += ("Total: " + totalPrice + " (yuan)\n");
        return itemsReceipt;
    }

}
